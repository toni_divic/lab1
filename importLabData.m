function [excite, drive, sense, sinksense, source] = importLabData(filename)
    % offsets
    sns_offset = 0.008; % volts
    drv_offset = 0.008; % volts
    % import and parse data
    data = importdata(filename);
    % extract ref V and conversion factors
    refV_str = data.textdata{1};
    refV_in_counts = str2double(refV_str((strfind(refV_str,'=') + 1):end));
    cnts_to_volts = 5 / refV_in_counts;
    cnts_to_volts_sinksense = (58.93 / 1000) / 4095;

    % gather data, offset, and convert to volts
    excite = (data.data(:,2) - data.data(1,2)) * cnts_to_volts;
    drive = (data.data(:,3) - drv_offset) * cnts_to_volts;
    sense = (data.data(:,4) - sns_offset) * cnts_to_volts;
    sinksense = (data.data(:,5) - data.data(1,5)) * cnts_to_volts_sinksense;
    source = (data.data(:,6) - sns_offset) * cnts_to_volts;
end



