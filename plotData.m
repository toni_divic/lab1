close all
%% Section 1
[excite, drive, sense, sinksense, source] = importLabData('Section1-9_combined_repeat.csv');
V_be = drive - sinksense;
V_ce = sense - sinksense;
I_b = (excite - drive) / 2200;
I_c = (source - sense) / 220;
P_q = V_be .* I_b + V_ce .* I_c;
Ic_to_Ib_ratio = I_c ./ I_b;

section_title = 'Section 1: ';
figure
plot(excite, V_be);
title([section_title 'V-be vs. Excite']);
xlabel('Excite (V)');
ylabel('V-be (V)');

figure
plot(excite, V_ce);
title([section_title 'V-ce vs. Excite']);
xlabel('Excite (V)');
ylabel('V-ce (V)');

figure
plot(excite, P_q);
title([section_title 'P-Q vs. Excite']);
xlabel('Excite (V)');
ylabel('P-Q (W)');

figure
plot(Ic_to_Ib_ratio, V_ce);
title([section_title 'V-ce vs. Ic:Ib']);
xlabel('Ic:Ib');
ylabel('V-ce (V)');

%% Section 2
[excite, drive, sense, sinksense, source] = importLabData('Section2-9_combined.csv');
V_gs = drive - sinksense;
V_ds = sense - sinksense;
I_g = (excite - drive) / 2200;
I_d = (source - sense) / 220;
P_q3 = V_gs .* I_g + V_ds .* I_d;
R_ds = V_ds ./ I_d;

section_title = 'Section 2: ';

figure
plot(excite, V_gs);
title([section_title 'V-gs vs. Excite']);
xlabel('Excite (V)');
ylabel('V-gs (V)');

figure
plot(excite, V_ds);
title([section_title 'V-ds vs. Excite']);
xlabel('Excite (V)');
ylabel('V-ds (V)');

figure
plot(excite, P_q3);
title([section_title 'P-Q3 vs. Excite']);
xlabel('Excite (V)');
ylabel('P-Q3 (W)');

figure
plot(excite, R_ds);
title([section_title 'R-ds(on) vs. Excite']);
xlabel('Excite (V)');
ylabel('R-ds (Ohms)');

%% Section 3
[excite, drive, sense, sinksense, drain] = importLabData('Section3-8_combined.csv');
V_gs = drive - sense;
V_ds = drain - sense;
I_g = (excite - drive) / 2200;
I_d = (drain - sinksense) / 220;
P_q6 = V_gs .* I_g + V_ds .* I_d;
R_ds = V_ds ./ I_d;

section_title = 'Section 3: ';

figure
plot(excite, V_gs);
title([section_title 'V-gs vs. Excite']);
xlabel('Excite (V)');
ylabel('V-gs (V)');

figure
plot(excite, V_ds);
title([section_title 'V-ds vs. Excite']);
xlabel('Excite (V)');
ylabel('V-ds (V)');

figure
plot(excite, P_q6);
title([section_title 'P-Q6 vs. Excite']);
xlabel('Excite (V)');
ylabel('P-Q6 (W)');

figure
plot(excite, R_ds);
title([section_title 'R-ds(on) vs. Excite']);
xlabel('Excite (V)');
ylabel('R-ds (Ohms)');

%% Section 4
[excite, drive, sense, sinksense, source] = importLabData('Section4-9_combined.csv');
V_gs = drive - sinksense;
V_ds = sense - sinksense;
I_g = (excite - drive) / 2200;
I_d = (source - sense) / 220;
P_q7 = V_gs .* I_g + V_ds .* I_d;
R_ds = V_ds ./ I_d;

section_title = 'Section 4: ';

figure
plot(excite, V_gs);
title([section_title 'V-gs vs. Excite']);
xlabel('Excite (V)');
ylabel('V-gs (V)');

figure
plot(excite, V_ds);
title([section_title 'V-ds vs. Excite']);
xlabel('Excite (V)');
ylabel('V-ds (V)');

figure
plot(excite, P_q7);
title([section_title 'P-Q7 vs. Excite']);
xlabel('Excite (V)');
ylabel('P-Q7 (W)');

figure
plot(excite, R_ds);
title([section_title 'R-ds(on) vs. Excite']);
xlabel('Excite (V)');
ylabel('R-ds (Ohms)');
